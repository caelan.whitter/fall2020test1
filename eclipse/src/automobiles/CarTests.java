package automobiles;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {

	@Test
	public void testCar() {
		
		try {
			Car car1 = new Car (-27);
			
			fail("The Car contrustor was supposed to throw an Exception");
		}
		
		catch (IllegalArgumentException e) {
			
		}
		
		catch (Exception e) {
			fail("The Rectangle through an exception of the wrong type");
		}
		
		
	}
	
	
	@Test
	public void testGetSpeed() {
		Car car2 = new Car(14);
		int result = car2.getSpeed();
		assertEquals(14,result);
	}
	
	@Test
	public void testGetLocation() {
		Car car4 = new Car(2);
		int result = car4.getLocation();
		assertEquals(50,result);
	}
	
	@Test
	public void testMoveRight() {
		Car car6 = new Car(56);
		car6.moveRight();
		int result = car6.getLocation();
		assertEquals(100,result);
		
	}
	
	@Test
	public void testMoveRight2() {
		Car car6 = new Car(23);
		car6.moveRight();
		int result = car6.getLocation();
		assertEquals(73,result);
		
	}
	
	
	@Test
	public void testMoveLeft() {
		Car car7 = new Car(58);
		car7.moveLeft();
		int result = car7.getLocation();
		assertEquals(0,result);
		
	}
	
	@Test
	public void testMoveLeft2() {
		Car car7 = new Car(4);
		car7.moveLeft();
		int result = car7.getLocation();
		assertEquals(46,result);
		
	}
	
	@Test
	public void testAccelerate() {
		Car car3 = new Car (18);
		car3.accelerate();
		int result = car3.getSpeed();
		assertEquals(19,result);
					
	}
	
	@Test
	public void testStop() {
		Car car5 = new Car (9);
		car5.stop();
		int result = car5.getSpeed();
		assertEquals(0,result);
	}
	
}
