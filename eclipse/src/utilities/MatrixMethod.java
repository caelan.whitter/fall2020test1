package utilities;

public class MatrixMethod {
	
	public static int[][] duplicate(int[][] matrix)
	{
		
		int length = matrix.length;
		int[][] newArr = new int[length][matrix[0].length*2];
		
		for (int i=0; i<matrix.length ; i++)
		{
			int z=0;
			for (int j=0; j<matrix[i].length*2; j=j+2)
			{
				newArr[i][j]=matrix[i][z];
				newArr[i][j+1]=matrix[i][z];
				z++;
			}
		}
		return newArr;
	}
	

}
