package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodTests {

	@Test
	public void testDuplicate() {
		int arr[][] = { {8,4,2,3},{9,0,1,7}};
		int arr2[][]=MatrixMethod.duplicate(arr);
		
		int result1=arr2[0][0];
		int result2=arr2[0][1];
		int result3=arr2[0][2];
		int result4=arr2[0][3];
		int result5=arr2[0][4];
		int result6=arr2[0][5];
		
		int result13=arr2[0][6];
		int result14=arr2[0][6];
		
		int result7=arr2[1][0];
		int result8=arr2[1][1];
		int result9=arr2[1][2];
		int result10=arr2[1][3];
		int result11=arr2[1][4];
		int result12=arr2[1][5];
		
		int result15=arr2[1][7];
		int result16=arr2[1][7];
		
		assertEquals(8,result1);
		assertEquals(8,result2);
		assertEquals(4,result3);
		assertEquals(4,result4);
		assertEquals(2,result5);
		assertEquals(2,result6);
		assertEquals(9,result7);
		assertEquals(9,result8);
		assertEquals(0,result9);
		assertEquals(0,result10);
		assertEquals(1,result11);
		assertEquals(1,result12);
		assertEquals(3,result13);
		assertEquals(3,result14);
		assertEquals(7,result15);
		assertEquals(7,result16);
		
		
	
	}

}
